import {Meteo} from './meteo';
import {GetMeteoService} from '../services/get-meteo.service';

export class Step {
  name: string;
  lat: number;
  lng: number;
  meteo: Meteo;

  constructor() {
    this.meteo = new Meteo();
  }

  Convert(value): number {
    return +value;
  }
}

