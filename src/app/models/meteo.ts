export class Meteo {

  sea_surface_temperature: number;
  wind_speed: number;

  constructor() {
    this.sea_surface_temperature = 0;
    this.wind_speed = 0;
  }
}
