import { TestBed, inject } from '@angular/core/testing';

import { GetMeteoService } from './get-meteo.service';

describe('GetMeteoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetMeteoService]
    });
  });

  it('should be created', inject([GetMeteoService], (service: GetMeteoService) => {
    expect(service).toBeTruthy();
  }));
});
