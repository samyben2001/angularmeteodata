import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Meteo} from '../models/meteo';

@Injectable()
export class GetMeteoService {

  constructor(private http: HttpClient) {
  }

  public getMeteo(lng, lat): Observable<any> {

    return this.http.get<Meteo>('https://api.planetos.com/v1/datasets/myocean_sst_europe_daily/' +
      'point?origin=dataset-details&lat=' + lat + '&apikey=31ca603fd2384e53ba2b72456fe481b9&lon=' + lng);
  }


}
