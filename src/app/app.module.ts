import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './components/app.component';
import {SearchComponent} from './components/search/search.component';
import {MapComponent} from './components/map/map.component';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {GetMeteoService} from './services/get-meteo.service';
import {HttpClientModule} from '@angular/common/http';
import {AgmCoreModule} from '@agm/core';
import { ChartWavesComponent } from './components/chart-waves/chart-waves.component';
import {ChartWindSpeedComponent} from './components/chart-wind-speed/chart-wind-speed.component';
import {ChartsModule} from 'ng2-charts';
import { ChartTemperatureComponent } from './components/chart-temperature/chart-temperature.component';

@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    MapComponent,
    ChartWavesComponent,
    ChartWindSpeedComponent,
    ChartTemperatureComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    ChartsModule,
    FormsModule,
    HttpClientModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDQv7PmLAEBDtUqEIkea2pL_yOY7gAUwZM'
    })
  ],

  providers: [
    GetMeteoService
  ],
  bootstrap: [AppComponent]
})

export class AppModule {
}


