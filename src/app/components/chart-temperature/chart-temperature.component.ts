import {Component, Input, OnInit} from '@angular/core';
import {Step} from '../../models/step';

@Component({
  selector: 'app-chart-temperature',
  templateUrl: './chart-temperature.component.html',
  styleUrls: ['./chart-temperature.component.css']
})
export class ChartTemperatureComponent implements OnInit {
  @Input()
  steps: Step[] = [];

  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          min: 0,
          max: 20
        }
      }]
    }
  };
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;

  public barChartLabels: string[] = [];
  public barChartData: any[] = [{data: [], label: 'T° surface'}];

  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  constructor() {
  }

  ngOnInit() {
    for (const step of this.steps) {
      this.barChartLabels.push(step.name);
      this.barChartData[0].data.push(step.meteo.sea_surface_temperature);
    }
  }
}
