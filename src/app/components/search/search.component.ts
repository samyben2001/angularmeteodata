import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import * as xml2js from 'xml2js';
import {Step} from '../../models/step';
import {GetMeteoService} from '../../services/get-meteo.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  @Output()
  notify: EventEmitter<Step[]> = new EventEmitter<Step[]>();
  steps: Step[] = [];
  Math: any;

  constructor(private getMeteo: GetMeteoService) {
    this.Math = Math;
  }

  ngOnInit() {
  }

  readInput(event: any): void {
    if (this.steps !== []) {
      const reader = new FileReader();

      reader.onload = (e: any) => {

        const xml = e.srcElement.result;

        xml2js.parseString(xml, (err, result) => {

          this.steps = [];
          let coords;
          const placemarks = result.kml.Document[0].Folder[0].Placemark;

          for (const placemark of placemarks) {
            const step = new Step();
            coords = placemark.Point[0].coordinates[0].split(',');
            step.lng = coords[0];
            step.lat = coords[1];
            step.name = placemark.name[0];
            this.steps.push(step);
          }
        });
        this.loadNextStep(0);
      };
      reader.readAsText(event.target.files[0]);
    }
  }

  private loadNextStep(index: number) {
    if (this.steps.length === index) {
      this.notify.emit(this.steps);
      return;
    }
    const step = this.steps[index];

    if (localStorage.getItem('meteo' + step.name) === null) {
      this.getMeteo.getMeteo(step.lng, step.lat).subscribe((meteo) => {
        this.steps[index].meteo.wind_speed = step.Convert(meteo.entries[0].data.wind_speed);
        this.steps[index].meteo.sea_surface_temperature = Math.round(step.Convert(meteo.entries[0].data.sea_surface_temperature) - 273.15);
        console.log(this.steps[index]);
        localStorage.setItem('meteo' + step.name, JSON.stringify(meteo));
      });
    } else {
      let windSpd = JSON.parse(localStorage.getItem('meteo' + step.name));
      windSpd = windSpd.entries[0].data.wind_speed;
      this.steps[index].meteo.wind_speed = windSpd;
      let surfaceTemp = JSON.parse(localStorage.getItem('meteo' + step.name));
      surfaceTemp = Math.round(surfaceTemp.entries[0].data.sea_surface_temperature - 273.15);
      this.steps[index].meteo.sea_surface_temperature = surfaceTemp;
    }
    index++;
    this.loadNextStep(index);
  }

}
