import {Component, OnInit} from '@angular/core';
import {GetMeteoService} from '../services/get-meteo.service';
import {Meteo} from '../models/meteo';
import {Step} from '../models/step';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  steps: Step[] = [];

  constructor() {
  }

  ngOnInit(): void {
  }

  onStepsReceived($event: Step[]) {
    // console.log($event);
    this.steps = $event;
  }
}


