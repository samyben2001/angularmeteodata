import {Component, Input, OnInit} from '@angular/core';
import {Step} from '../../models/step';

@Component({
  selector: 'app-chart-waves',
  templateUrl: './chart-waves.component.html',
  styleUrls: ['./chart-waves.component.css']
})
export class ChartWavesComponent implements OnInit {
  @Input()
  steps: Step[] = [];

  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;
  public barChartLabels: string[] = ['Step1', 'Step2', 'Step3', 'Step4', 'Step5', 'Step6', 'Step7', 'Step8', 'Step9'];
  public barChartData: any[] = [
    {data: [80, 59, 80, 81, 56, 55, 40], label: 'Taille des vagues'},
    // {data: [28, 48, 40, 19, 86, 27, 90], label: 'T° de l\'air'}
  ];

  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  constructor() {
  }

  ngOnInit() {
  }

}
