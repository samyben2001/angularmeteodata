import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartWavesComponent } from './chart-waves.component';

describe('ChartWavesComponent', () => {
  let component: ChartWavesComponent;
  let fixture: ComponentFixture<ChartWavesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartWavesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartWavesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
