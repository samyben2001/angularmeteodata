import {Component, Input, OnInit} from '@angular/core';
import {Step} from '../../models/step';


@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  Math: any;
  @Input()
  steps: Step[] = [];

  constructor() {
    this.Math = Math;
  }

  Convert(value): number {
    return +value;
  }

  ngOnInit() {
  }
}
