///<reference path="../../../../node_modules/@angular/core/src/metadata/directives.d.ts"/>
import {Component, Input, OnInit} from '@angular/core';
import {Step} from '../../models/step';

@Component({
  selector: 'app-chart-wind-speed',
  templateUrl: './chart-wind-speed.component.html',
  styleUrls: ['./chart-wind-speed.component.css']
})
export class ChartWindSpeedComponent implements OnInit {

  @Input()
  steps: Step[] = [];

  data: any = {};

  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          min: 0,
          max: 15
        }
      }]
    }
  };
  public barChartType = 'bar';
  public barChartLegend = true;
  public barChartLabels: string[] = [];
  public barChartData: any[] = [{
    data: [],
    label: 'Vitesse du vent',
    backgroundColor: ''
  }];

  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  constructor() {
  }

  // setData() {
  //
  //   for (const step of this.steps) {
  //     this.barChartData[0].data.push(step.meteo.wind_speed);
  //   }
  //   return this.barChartData;
  // }
  //
  // setLabels() {
  //   for (const step of this.steps) {
  //     console.log(this.steps);
  //     this.barChartLabels.push(step.name);
  //   }
  //   return this.barChartLabels;
  // }

  ngOnInit() {
    for (const step of this.steps) {
      this.barChartLabels.push(step.name);
      this.barChartData[0].data.push(step.meteo.wind_speed);
      this.barChartData[0].backgroundColor = '#085500';
    }
  }

}
