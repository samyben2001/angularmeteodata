import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartWindSpeedComponent } from './chart-wind-speed.component';

describe('ChartWindSpeedComponent', () => {
  let component: ChartWindSpeedComponent;
  let fixture: ComponentFixture<ChartWindSpeedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartWindSpeedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartWindSpeedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
